<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateKeyDuplicate</fullName>
        <field>KeyCheckDuplicateAppraiser__c</field>
        <formula>Employee__c + TEXT( DateWorkFrom__c )</formula>
        <name>UpdateKeyDuplicate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>DuplicateAppraiserInDay</fullName>
        <actions>
            <name>UpdateKeyDuplicate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( NOT(IsDeleted__c),OR(ISCHANGED( Employee__c ),ISCHANGED( DateWorkFrom__c ),ISCHANGED( IsDeleted__c) ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
