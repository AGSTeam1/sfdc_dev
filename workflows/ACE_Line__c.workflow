<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateDuplicateLineByArea</fullName>
        <field>DuplicateLineByArea__c</field>
        <formula>IF( ShukkinType__c =&apos;稼働無し&apos;, &apos;KDNS-&apos;, &apos;KD-&apos;) + TEXT( DateOfLine__c )+  Area__c + TEXT( LineNumber__c)</formula>
        <name>UpdateDuplicateLineByArea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateDuplicateLineByEmp</fullName>
        <field>DuplicateLineByEmp__c</field>
        <formula>IF( ShukkinType__c =&apos;稼働無し&apos;, &apos;KDNS-&apos;, &apos;KD-&apos;)+ Area__c + TEXT(DateOfLine__c) +  Employee__c</formula>
        <name>UpdateDuplicateLineByEmp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Line_Problem_Status</fullName>
        <field>IsProblem__c</field>
        <literalValue>0</literalValue>
        <name>Update Line Problem Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Warning_Remark</fullName>
        <field>WarningRemark__c</field>
        <formula>&apos;&apos;</formula>
        <name>Update Warning Remark</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>CheckLineProblem</fullName>
        <actions>
            <name>Update_Line_Problem_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Warning_Remark</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(CountAPO__c &lt;= MaxApo__c, IF(!ISBLANK(MinAPOTime__c), MinAPOTime__c &gt;= WorkTimeFrom__c,TRUE), IF(!ISBLANK(MaxAPOTime__c), MaxAPOTime__c &lt;= WorkTimeTo__c,TRUE))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DuplicateLineByEmployeeAtArea</fullName>
        <actions>
            <name>UpdateDuplicateLineByArea</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateDuplicateLineByEmp</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(NOT(IsDeleted__c),NOT( isExtraLine__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
